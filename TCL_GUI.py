# http://www.binarytides.com/python-socket-programming-tutorial/
import socket
import sys
from Tkinter import *
import pickle
from time import sleep
import datetime
import threading

def onExit():
    # Write Defaults
    writeDefs(d)
    # Close the socket
    s.close()
    dispText('Socket Closed')
    root.destroy()

class DefValObj(object):
    # Object to store default values
    def __init__(self):
        self.ip = '192.168.31.95' # P120
        self.port = '7360' # TCL Port
        self.mot = "pogo"
        self.cycleno = 30
        self.r = 50
        self.g = 50
        self.b = 50
        self.moveRel1 = 0;
        self.moveRel2 = 0;
        self.moveAbs1 = 0;
        self.moveAbs2 = 0;
        self.moveAbs3 = 0;
        self.fanpwm = 100;
        self.unit = "";
        self.gantryX = 67322;
        self.gantryY = 119299;
        self.gantryCycles = 1;

def readDefs(defVals):
    # Load Defaults from file
    try:
        with open('def.txt', 'r') as myfile:
           defVals  = pickle.load(myfile)
    except IOError, EOFError:
        dispText("No default file or defaults found.  Using init values")
    return defVals

def writeDefs(defVals):
    with open('def.txt', 'w') as myfile:
        defVals.ip = ipVar.get()
        defVals.port = portVar.get()
        defVals.mot = motVar.get()
        defVals.cycleno = cycleNoVar.get()
        defVals.r = rVar.get()
        defVals.g = gVar.get()
        defVals.b = bVar.get()
        defVals.fanpwm = fanpwmVar.get()
        defVals.moveRel1 = moveRel1Var.get()
        defVals.moveRel2 = moveRel2Var.get()
        defVals.moveAbs1 = moveAbs1Var.get()
        defVals.moveAbs2 = moveAbs2Var.get()
        defVals.moveAbs3 = moveAbs3Var.get()
        defVals.unit = unitVar.get()
        defVals.gantryX = gantryXVar.get()
        defVals.gantryY = gantryYVar.get()
        defVals.gantryCycles = gantryCyclesVar.get()
        
        pickle.dump(defVals, myfile)#, pickle.HIGHEST_PROTOCOL)

def tcls(msg):
    if connected.get():     
        msg += '\r\n'
        try:
            s.send(msg)
            dispText(msg)
        except socket.error:
            dispText('Send failed')
            sys.exit()
    else:
        dispText("Not connected")
        
def tclSendScript(msg):
    if connected.get():     
        msg += '\r\n'
        try:
            s.send(msg)
        except socket.error:
            dispText('Send failed')
            sys.exit()
    else:
        dispText("Not connected")

def continuousRead():
    while True:
        reply = s.recv(4096)
        recvt.insert(END, reply)
        recvt.see("end")

def dispText(msg):
    recvt.insert(END, msg)
    recvt.insert(END, "\r\n")

def clearText():
    recvt.delete(1.0, END)

def _copyb():
    content=recvt.get(1.0, END)
    root.clipboard_clear()
    root.clipboard_append(content)
















############## Initialization

# Create Tkinter GUI Window
root = Tk()
root.protocol('WM_DELETE_WINDOW', onExit) # Call onexit when window closed

# Comment on versioning
windowTitle = "FF LP TCL GUI 1.14"
root.wm_title(windowTitle)
Label(root, text="Compatible with FW ver: 56.1").grid(row=0, column=6, sticky=NW)

# Misc
Button(root, text="Write Defaults", command=lambda: writeDefs(d)).grid(row=0, column=5, sticky=NE)
hilightbg = 'light blue'
dimfg = '#8b8989'


# Create Main Text Widget
recvf = Frame(root, padx=10, pady=10)
recvf.grid(row=1, column=5, columnspan=100, rowspan=7)

recvt = Text(recvf, width=70, height=35)#, state=DISABLED)
recvt.grid(row=3, column=0, columnspan=10)
recvscr = Scrollbar(recvf)
recvt.config(yscrollcommand=recvscr.set)
recvscr.config(command=recvt.yview)
recvscr.grid(row=3, column=11, sticky=NS)
#Button(recvf, text="Clear All Text", command=clearText).grid(row=2, column=0)

# Load defaults
d = DefValObj()
d = readDefs(d) # Load defaults from file

def _writeTraceb():
    contents = recvt.get(1.0, END)
    traceFileName = "TraceResults\\"+unitVar.get()+"_"+subsystemVar.get()+"_TRACE_"+suffixVar.get()+".txt"
    with open(traceFileName, 'w') as tracefile:
        tracefile.write(contents)
    suffixVar.set("")
    

def _writeTextb():
    contents = recvt.get(1.0, END)
    now = datetime.datetime.now()
##    ts = str(now.year)+str(now.month)+str(now.day)+"_"+str(now.minute)+str(now.second)
    ts = now.strftime('%Y%m%d_%H%M%S')
    textFileName = "logs\\LPP1_"+unitVar.get()+"_"+ts+"_"+textFileNameVar.get()+".txt"
    with open(textFileName, 'w') as textfile:
        textfile.write(contents)

def _sendb():
    tcls(sendVar.get())
    sendVar.set("")

Label(recvf, text="Filename Suffix:").grid(row=0, column=1)
suffixVar=StringVar()
suffixVar.set("")
Entry(recvf, textvariable=suffixVar).grid(row=1, column=1)
Button(recvf, text="Write TRACE to File", command=_writeTraceb, bg=hilightbg).grid(row=2, column=1)

Label(recvf, text="Filename Suffix:").grid(row=0, column=2)
textFileNameVar = StringVar()
textFileNameVar.set("")
Entry(recvf, textvariable=textFileNameVar).grid(row=1, column=2)
Button(recvf, text="Write Text to File", command=_writeTextb, bg=hilightbg).grid(row=2, column=2)

Button(recvf, text="Copy All to Clipboard", command=_copyb, bg=hilightbg).grid(row=2,column=3)

sendVar = StringVar()
sendVar.set("")
Entry(recvf, textvariable=sendVar).grid(row=4, column=1, columnspan=9, sticky='ew')
Button(recvf, text="Send", command=_sendb, bg='green').grid(row=4, column=0)


Button(recvf, text="Clear All", command=clearText).grid(row=5, column=9)



# Create a socket, s
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error, msg:
    dispText('Failed to create socket. Error code: ' + str(msg[0]) + ',  Error message: ' + msg[1])
    sys.exit();
dispText('Socket Created')

# Reading thread
t = threading.Thread(target=continuousRead)
t.daemon = True











############ Connecting
ipbg = 'deep sky blue'
ipf = Frame(root, bg=ipbg)
ipf.grid(row=0, column=0, sticky=NW)

def _connectb():
    if ipVar.get()!= "" and portVar.get()!= "" and unitVar.get() != "":        
        # Connect to the remote server via s
        remote_ip = ipVar.get()
        port = int(portVar.get())
        s.connect((remote_ip, port))
        dispText('Socket connected on ip ' + remote_ip + ' on port ' + str(port))
        connected.set(TRUE)
        t.start() # Start read thread
        root.wm_title(windowTitle+" - Unit "+str(unitVar.get()))
    else:
        dispText('Please fill in all "Connect" fields')

Label(ipf, text="IP:", bg=ipbg).grid(row=0, column=0)
ipVar = StringVar()
ipVar.set(d.ip)
Entry(ipf, textvariable=ipVar).grid(row=0,column=1)

Label(ipf, text="Port:", bg=ipbg).grid(row=1, column=0)
portVar = StringVar()
portVar.set(str(d.port))
Entry(ipf, textvariable=portVar).grid(row=1, column=1)

Label(ipf, text="Unit:", bg=ipbg).grid(row=2, column=0)
unitVar = StringVar()
unitVar.set("")#d.unit)
Entry(ipf, textvariable=unitVar).grid(row=2, column=1)

connected = BooleanVar() #Variable storing info on whether or not connected
connected.set(FALSE)
Button(ipf, text="Connect", command=_connectb, bg=hilightbg, width=9, height=4).grid(row=0, column=2, rowspan=3)













############ Utilities
utf = Frame(root)
utf.grid(row=0, column=1, columnspan=3, sticky=NW)

def _idDumpb():
    tcls('id dump')
    sleep(.01)
    tcls('ls')

def _sendScriptb():
    # Send script file
    if connected.get():
        fname = 'SimplexityTestScripts.txt'
        with open(fname, 'r') as sf:        
            for i, l in enumerate(sf):
                pass
            lengthf = i + 1
        with open(fname, 'r') as sf:
            for i, l in enumerate(sf):
                tclSendScript(l)
                sleep(.01)
                #dispText("Sent line " + str(i+1) + " of " + str(lengthf))
        dispText("Script Sent Successfully")
    else:
        dispText("Not connected")
        
def _initb():
    tcls('testinit')
##    tcls('lp op init')

def _debugOnb():
    tcls('log debug io 1')
    sleep(.01)
    tcls(' log debug lp 1')

def _debugOffb():
    tcls('log debug io 0')
    sleep(.01)
    tcls(' log debug lp 0')

Button(utf, text="ID Dump", command=_idDumpb, height=3, bg=hilightbg).grid(row=0,column=0, rowspan=3)
Button(utf, text="Send Script", command=_sendScriptb, height=3, bg=hilightbg).grid(row=0, column=1, rowspan=3)
Button(utf, text="Initialize", command=lambda: tcls('testinit'), height=3, bg=hilightbg).grid(row=0,column=2, rowspan=3)
Button(utf, text="Set PID", command=lambda:tcls('setpid_all'), height=3).grid(row=0, column=3)
Button(utf, text="Turn On\nDebug", command=_debugOnb, height=3).grid(row=0, column=4)
Button(utf, text="Turn Off\nDebug", command=_debugOffb, height=3).grid(row=0, column=5)











############ Motors
## Choose motors
motbg = 'light coral'
motf = Frame(root, bg=motbg, padx=5, pady=5)
motf.grid(row=3, column=0, sticky=NW, rowspan=2)


def _moveRel1b():
    mot = motVar.get()
    targetpos = moveRel1Var.get()
    tcls('motor move '+mot+' '+targetpos)

def _moveRel2b():
    mot = motVar.get()
    targetpos = moveRel2Var.get()
    tcls('motor move '+mot+' '+targetpos)

def _moveAbs1b():
    mot = motVar.get()
    targetpos = moveAbs1Var.get()
    tcls('motor moveabs '+mot+' '+targetpos)

def _moveAbs2b():
    mot = motVar.get()
    targetpos = moveAbs2Var.get()
    tcls('motor moveabs '+mot+' '+targetpos)

def _moveAbs3b():
    mot = motVar.get()
    targetpos = moveAbs3Var.get()
    tcls('motor moveabs '+mot+' '+targetpos)

def _motPosb():
    mot = motVar.get()
    tcls('motor pos '+mot)

def _motParmb():
    mot = motVar.get()
    tcls('motor parm '+mot)

Label(motf, text="Choose Motor:", bg=motbg).grid(row=0, column=0)
mots = ["gantry_x", "gantry_y", "gantry_z", "magnet", "tec", "pogo", "cart"]
motVar = StringVar()
motVar.set(d.mot)
for idx, val in enumerate(mots):
    motb = Radiobutton(motf, text=val, variable=motVar, value=val, bg=motbg)
    motb.grid(row=idx+1, column=0, sticky="NW")

## Move distances
# String variables
moveRel1Var = StringVar()
moveRel2Var = StringVar()
moveAbs1Var = StringVar()
moveAbs2Var = StringVar()
moveAbs3Var = StringVar()
moveRel1Var.set(d.moveRel1)
moveRel2Var.set(d.moveRel2)
moveAbs1Var.set(d.moveAbs1)
moveAbs2Var.set(d.moveAbs2)
moveAbs3Var.set(d.moveAbs3)

# Entry Widgets
Entry(motf, textvariable=moveRel1Var, width=8).grid(row=1,column=1)
Entry(motf, textvariable=moveRel2Var, width=8).grid(row=2,column=1)
Entry(motf, textvariable=moveAbs1Var, width=8).grid(row=3,column=1)
Entry(motf, textvariable=moveAbs2Var, width=8).grid(row=4,column=1)
Entry(motf, textvariable=moveAbs3Var, width=8).grid(row=5,column=1)

# Buttons
Button(motf, text="Move Relative", command=_moveRel1b).grid(row=1, column=2)
Button(motf, text="Move Relative", command=_moveRel2b).grid(row=2, column=2)
Button(motf, text="Move Absolute", command=_moveAbs1b).grid(row=3, column=2)
Button(motf, text="Move Absolute", command=_moveAbs2b).grid(row=4, column=2)
Button(motf, text="Move Absolute", command=_moveAbs3b).grid(row=5, column=2)

# Motor General
Button(motf, text="Motor\nPosition", command=_motPosb, bg=hilightbg, pady=5).grid(row=9, column=0)
Button(motf, text="Motor\nParameters", command=_motParmb, bg=hilightbg, pady=5).grid(row=9, column=1)



######### Common Motor Routines
combg='indian red'
comf = Frame(root, bg=combg, padx=5, pady=5)
comf.grid(row=3, column=1, rowspan=2, columnspan=4, sticky=NW)

Label(comf, text='Common Routines:', bg=combg).grid(row=0, column=0)
Button(comf, text="Home Gantry Z", command=lambda: tcls('home gantry_z 100000 gantryZ_home 0')).grid(row=1, column=0)
Button(comf, text="Home Gantry Y", command=lambda: tcls('home gantry_y -200000 gantryY_home 0')).grid(row=2, column=0)
Button(comf, text="Home Gantry X", command=lambda: tcls('home gantry_x -200000 gantryX_home 0')).grid(row=3, column=0)
Button(comf, text="Home Magnet", command=lambda: tcls('home magnet 50000 magnet_lifthome 1')).grid(row=4, column=0)
Button(comf, text="Home TEC", command=lambda: tcls('home tec 150000 tec_lifthome 0')).grid(row=5, column=0)
Button(comf, text="Home Pogo", command=lambda: tcls('home pogo -900000 pogo_home 1')).grid(row=6, column=0)
Button(comf, text="Home Cartridge", command=lambda: tcls('home cart 100000 cart_home 1')).grid(row=7, column=0)

Button(comf, text="Disengage", command=lambda: tcls('disengage'), bg=hilightbg).grid(row=8, column=0)
Button(comf, text="Engage Pogo/TEC", command=lambda: tcls('engage'), bg=hilightbg).grid(row=8, column=1)

Button(comf, text="Disengage Magnet", command=lambda: tcls('motor moveabs magnet -4706')).grid(row=4, column=1)
Button(comf, text="Engage Magnet Front", command=lambda: tcls('motor moveabs magnet 16911')).grid(row=4, column=2)
Button(comf, text="Engage Magnet Rear", command=lambda: tcls('motor moveabs magnet -15142')).grid(row=4, column=3)
Button(comf, text="Disengage TEC", command=lambda: tcls('motor moveabs tec -9626')).grid(row=5, column=1)
Button(comf, text="Engage TEC", command=lambda: tcls('motor moveabs tec 110695')).grid(row=5, column=2)
Button(comf, text="Disengage Pogo", command=lambda: tcls('motor moveabs pogo 10165')).grid(row=6, column=1)
Button(comf, text="Engage Pogo", command=lambda: tcls('motor move pogo -900000 pogo_compressionhome 1 1000')).grid(row=6, column=2)
Button(comf, text="Engage Pogo (B1)", command=lambda: tcls('motor move pogo -900000 pogo_compressionhome 0 1000'), fg=dimfg).grid(row=6, column=3)
Button(comf, text="Load Cartridge", command=lambda: tcls('motor moveabs cart -356')).grid(row=7, column=1)
Button(comf, text="Unload Cartridge", command=lambda: tcls('motor moveabs cart -89449')).grid(row=7, column=2)
Button(comf, text="Cartridge Sample Fill", command=lambda: tcls('motor moveabs cart -80504')).grid(row=7, column=3)



######### Gantry Movments
ganbg='gold'
ganf = Frame(comf, bg=ganbg, padx=5, pady=5)
ganf.grid(row=0, column=1, rowspan=4, columnspan=3)

def _xyLocb():
    tcls('xyloc '+xyLocVar.get())
    dispText('sending')
    counter = int(xyLocVar.get())
    counter += 1;
    if (counter > 8):
        counter = 1
    xyLocVar.set(str(counter))

def _offsetb():
    tcls('xyoffset '+xOffVar.get()+' '+yOffVar.get())
    


gantryXVar = StringVar()
gantryYVar = StringVar()
gantryXVar.set(d.gantryX)
gantryYVar.set(d.gantryY)
Label(ganf, text="Gantry:", bg=ganbg).grid(row=0, column=0, pady=5, columnspan=2)
Label(ganf, text="X:", bg=ganbg).grid(row=1, column=0)
Label(ganf, text="Y:", bg=ganbg).grid(row=2, column=0)
Entry(ganf, textvariable=gantryXVar, width=7).grid(row=1, column=1, padx=5)
Entry(ganf, textvariable=gantryYVar, width=7).grid(row=2, column=1, padx=5)
Button(ganf, text="Move Gantry\nto Position", command=lambda: tcls('gantryXY '+gantryXVar.get()+' '+gantryYVar.get())).grid(row=1, column=2, rowspan=2)
Button(ganf, text="Pierce", command=lambda: tcls('pierce'), height=2).grid(row=1, column=3, rowspan=2)

gantryCyclesVar = StringVar()
gantryCyclesVar.set(d.gantryCycles)
Label(ganf, text="Num Cycles:", bg=ganbg).grid(row=0, column=5)
Entry(ganf, textvariable=gantryCyclesVar, width=3).grid(row=0, column=6)
Button(ganf, text="Test Gantry", command=lambda: tcls('gantrytest '+gantryCyclesVar.get()), bg=hilightbg).grid(row=0, column=4, padx=5)

xOffVar = StringVar()
yOffVar = StringVar()
Button(ganf, text="Set Offset (mm):", command=_offsetb).grid(row=1, column=4)
Entry(ganf, textvariable=xOffVar, width=6).grid(row=1, column=5)
Entry(ganf, textvariable=yOffVar, width=6).grid(row=1, column=6)

xyLocVar = StringVar()
xyLocVar.set(1)
Label(ganf, text="XY Test Loc:", bg=ganbg).grid(row=2, column=5)
Entry(ganf, textvariable=xyLocVar, width=3).grid(row=2, column=6)
Button(ganf, text="Move to XY Loc", command=_xyLocb).grid(row=2, column=4)





############ Trace Test
tracebg = 'green'
tracef = Frame(root, bg=tracebg, padx=5, pady=5)
tracef.grid(row=5, column=0, sticky=NW, rowspan=2)

subsystemVar = StringVar()
subsystemVar.set("No Subsystem")


def _traceCartb():
    clearText()
    tcls('tracemot "Cartridge Load" cart -89449 -356')
    subsystemVar.set("Cartridge_Load")

def _tracePogob():
    clearText()
    tcls('tracemot "Pogo Actuator" pogo 0 -625129')
    subsystemVar.set("Pogo_Actuator")

def _traceTECb():
    clearText()
    tcls('tracemot "TEC Engage" tec 0 110695')
    subsystemVar.set("TEC_Engage")

def _traceFrontMagb():
    clearText()
    tcls('tracemot "Engage Magnet Front" magnet -4706 16911')
    subsystemVar.set("Engage_Magnet_Front")

def _traceRearMagb():
    clearText()
    tcls('tracemot "Engage Magnet Rear" magnet -4706 -15142')
    subsystemVar.set("Engage_Magnet_Rear")

def _traceGantryXb():
    clearText()
    tcls('tracemot "Gantry X" gantry_x -70991 68613')
    subsystemVar.set("Gantry_X")

def _traceGantryYb():
    clearText()
    tcls('tracemot "Gantry Y" gantry_y 121297 7223')
    subsystemVar.set("Gantry_Y")

def _traceGantryZb():
    clearText()
    tcls('tracemot "Gantry Z" gantry_z 0 50000')
    subsystemVar.set("Gantry_Z")

def _traceFrontMagRetractb():
    clearText()
    tcls('tracemot "Retract Magnet Front" magnet 16911 -4706')
    subsystemVar.set("Retract_Magnet_Front")

def _traceRearMagRetractb():
    clearText()
    tcls('tracemot "Retract Magnet Rear" magnet -15142 -4706')
    subsystemVar.set("Retract_Magnet_Rear")

Label(tracef, text="Trace Tools:", bg=tracebg).grid(row=0, column=0)
Button(tracef, text="Trace Cartridge Load", command=_traceCartb).grid(row=1,column=0)
Button(tracef, text="Trace Pogo Actuator", command=_tracePogob).grid(row=2,column=0)
Button(tracef, text="Trace TEC", command=_traceTECb).grid(row=3,column=0)
Button(tracef, text="Trace Magnet Front", command=_traceFrontMagb).grid(row=4,column=0)
Button(tracef, text="Trace Magnet Rear", command=_traceRearMagb).grid(row=5,column=0)
Button(tracef, text="Trace Gantry X", command=_traceGantryXb).grid(row=8,column=0)
Button(tracef, text="Trace Gantry Y", command=_traceGantryYb).grid(row=9,column=0)
Button(tracef, text="Trace Gantry Z", command=_traceGantryZb).grid(row=10,column=0)
Button(tracef, text="Trace Magnet Front Retract", command=_traceFrontMagRetractb, fg=dimfg).grid(row=11,column=0)
Button(tracef, text="Trace Magnet Rear Retract", command=_traceRearMagRetractb, fg=dimfg).grid(row=12,column=0)

##Button(tracef, text="Write Trace to File", command=writeTrace, bg=hilightbg).grid(row=9, column=0)

##Button(tracef, text="Pogo Compression\nSensor State", command=_pogoCompressSensorb).grid(row=2, column=1, rowspan=2)





############# Cycle Test
cyclebg='orange'
cyclef = Frame(root, bg=cyclebg, padx=5, pady=5)
cyclef.grid(row=5, column=1, sticky=NW, rowspan=2)
cycleNoVar = IntVar() # Stores number of cycles
cycleNoVar.set(d.cycleno)
    
    

Label(cyclef, text="Cycle Tools:", bg=cyclebg).grid(row=0, column=0)
Button(cyclef, text="Cycle Cartridge Load", command=lambda: tcls('cycle "Cartridge Loading" cart -89449 -356 '+str(cycleNoVar.get()))).grid(row=1, column=0)
Button(cyclef, text="Cycle Pogo Actuator", command=lambda: tcls('cycle "Pogo Actuator" pogo 0 -625129 '+str(cycleNoVar.get()))).grid(row=2, column=0)
Button(cyclef, text="Cycle TEC", command=lambda: tcls('cycle "TEC Engage" tec 0 110695 '+str(cycleNoVar.get()))).grid(row=3, column=0)
Button(cyclef, text="Cycle Magnet Front", command=lambda: tcls('cycle "Engage Magnet Front" magnet -4706 16911 '+str(cycleNoVar.get()))).grid(row=4, column=0)
Button(cyclef, text="Cycle Magnet Rear", command=lambda: tcls('cycle "Engage Magnet Rear" magnet -4706 -15142 '+str(cycleNoVar.get()))).grid(row=5, column=0)
Button(cyclef, text="Cycle Gantry X", command=lambda: tcls('cycle "Gantry X" gantry_x -70991 68613 '+str(cycleNoVar.get()))).grid(row=6, column=0)
Button(cyclef, text="Cycle Gantry Y", command=lambda: tcls('cycle "Gantry Y" gantry_y 121297 7223 '+str(cycleNoVar.get()))).grid(row=7, column=0)
Button(cyclef, text="Cycle Gantry Z", command=lambda: tcls('cycle "Gantry Z" gantry_z 0 50000 '+str(cycleNoVar.get()))).grid(row=8, column=0)


Button(cyclef, text="Cycle Pogo\n and TEC", command=lambda:tcls('cycle_pogo_tec '+str(cycleNoVar.get()))).grid(row=2, column=1, rowspan=2)
Button(cyclef, text="Cycle Each\nMagnet", command=lambda:tcls('cycle_magnets '+str(cycleNoVar.get()))).grid(row=4, column=1, rowspan=2)
Button(cyclef, text="Cycle Each\nGantry", command=lambda:tcls('cycle_gantrys '+str(cycleNoVar.get()))).grid(row=6, column=1, rowspan=3)
Button(cyclef, text="Cycle All\nAfter Load", command=lambda: tcls('cycle_all '+str(cycleNoVar.get()))).grid(row=10, column=1)

Label(cyclef, text="Number of Cycles", bg=cyclebg).grid(row=0, column=1)
Entry(cyclef, textvariable=cycleNoVar, width=5).grid(row=1, column=1, padx=5)









############# UX Testing
uxbg = 'light blue'
uxf = Frame(root, bg=uxbg, padx=5, pady=5)
uxf.grid(row=5, column=2, sticky=NW)

def _uxb():
    message = 'can gcmd 0x107 3 '+str(rVar.get())+' '+str(gVar.get())+' '+str(bVar.get())
    tcls(message)

def _lightb():
    message = 'can gcmd 0x100 3 '+str(rVar.get())+' '+str(gVar.get())+' '+str(bVar.get())
    tcls(message)

def _lightOffb():
    tcls('can gcmd 0x107 3 0 0 0')
    sleep(.01)
    tcls('can gcmd 0x100 3 0 0 0')

Label(uxf, text="UX Tools:", bg=uxbg).grid(row=0, column=0, columnspan=3)
Label(uxf, text="R", bg=uxbg).grid(row=1, column=0, sticky=S)
Label(uxf, text="G", bg=uxbg).grid(row=1, column=1, sticky=S)
Label(uxf, text="B", bg=uxbg).grid(row=1, column=2, sticky=S)
rVar = IntVar()
gVar = IntVar()
bVar = IntVar()
rVar.set(d.r)
gVar.set(d.g)
bVar.set(d.b)
Entry(uxf, textvariable=rVar, width=3).grid(row=2, column=0, sticky=NW, pady=5)
Entry(uxf, textvariable=gVar, width=3).grid(row=2, column=1, sticky=NW, pady=5)
Entry(uxf, textvariable=bVar, width=3).grid(row=2, column=2, sticky=NW, pady=5)

Button(uxf, text="Send to\nUX band", width=10, command=_uxb).grid(row=3, column=0, columnspan=3)
Button(uxf, text="Send to\nLight band", width=10, command=_lightb).grid(row=4, column=0, columnspan=3)
Button(uxf, text="Turn off\nall", width=10, command=_lightOffb).grid(row=5, column=0, columnspan=3)





############ Fan Testing
fanbg = 'pink'
fanf = Frame(root, padx=5, pady=5, bg=fanbg)
fanf.grid(row=5, column=3, sticky=NW)

def _fanb(fanNo):
    message = 'can fan '+str(fanNo)+' '+str(fanpwmVar.get())
    tcls(message)

def _fanOffb():
    tcls('can fan 1 0')
    sleep(.01)
    tcls('can fan 2 0')
    sleep(.01)
    tcls('can fan 3 0')
    sleep(.01)
    tcls('can fan 4 0')
    sleep(.01)
    tcls('can fan 5 0')

Label(fanf, text="Fan Tools:", bg=fanbg).grid(row=0, column=0, columnspan=3)
fanpwmVar = IntVar()
fanpwmVar.set(d.fanpwm)
Entry(fanf, textvariable=fanpwmVar, width=3).grid(row=2, column=1)
Button(fanf, text="Send to Fan1", command=lambda: _fanb(1)).grid(row=3, column=0, columnspan=3)
Button(fanf, text="Send to PCR Fan", command=lambda: _fanb(2)).grid(row=4, column=0, columnspan=3)
Button(fanf, text="Send to Left Inlet Fan", command=lambda: _fanb(3)).grid(row=5, column=0, columnspan=3)
Button(fanf, text="Send to Right Inlet Fan", command=lambda: _fanb(4)).grid(row=6, column=0, columnspan=3)
Button(fanf, text="Send to TEC Fan", command=lambda: _fanb(5)).grid(row=7, column=0, columnspan=3)
Button(fanf, text="Turn Off All", command=_fanOffb).grid(row=8, column=0, columnspan=3)


############## TEC Testing
tecbg = 'plum'
tecf = Frame(root, padx=5, pady=5,bg=tecbg)
tecf.grid(row=6, column=3, sticky=NW)

Label(tecf, text="TEC Functionality Tools:", bg=tecbg).grid(row=0, column=0)
Button(tecf, text="Turn On TEC Fan", command=lambda: tcls('can fan 5 100')).grid(row=1, column=0)
Button(tecf, text="Turn On TEC", command=lambda: tcls('can gcmd 0x10c 1 90')).grid(row=2, column=0)
Button(tecf, text="Turn Off TEC", command=lambda: tcls('can gcmd 0x10c 1 0')).grid(row=3, column=0)
Button(tecf, text="Turn Off TEC Fan", command=lambda: tcls('can fan 5 0')).grid(row=4, column=0)











############# Sensor Queries
sensbg = 'spring green'
sensf = Frame(root, padx=5, pady=5, bg=sensbg)
sensf.grid(row=7, column=0, sticky=NW, columnspan=100)

Label(sensf, text="Sensor Queries:", bg=sensbg).grid(row=0, column=0)
Button(sensf, text="IO Dump", command=lambda:tcls('io dump'), bg=hilightbg, height=2).grid(row=1, column=0)
Button(sensf, text="Gantry X", command=lambda: tcls('io access gantryX_home'), height=2).grid(row=1, column=1)
Button(sensf, text="Gantry Y", command=lambda: tcls('io access gantryY_home'), height=2).grid(row=1, column=2)
Button(sensf, text="Gantry Z", command=lambda: tcls('io access gantryZ_home'), height=2).grid(row=1, column=3)
Button(sensf, text="Magnet", command=lambda: tcls('io access magnet_lifthome'), height=2).grid(row=1, column=4)
Button(sensf, text="TEC", command=lambda: tcls('io access tec_lifthome'), height=2).grid(row=1, column=5)
Button(sensf, text="Pogo", command=lambda: tcls('io access pogo_home'), height=2).grid(row=1, column=6)
Button(sensf, text="Pogo\nCompression", command=lambda: tcls('io access pogo_compressionhome'), height=2).grid(row=1, column=7)
Button(sensf, text="Cartridge\nHome", command=lambda: tcls('io access cart_home'), height=2).grid(row=1, column=8)
Button(sensf, text="Cartridge\nLoaded", command=lambda: tcls('io access cartridge_loaded'), height=2).grid(row=1, column=9)
Button(sensf, text="Cartridge\nPresent", command=lambda: tcls('io access cartridge_present'), height=2).grid(row=1, column=10)







root.mainloop()
