%% Servo Tuning Plotter
% File that lets you choose a text file from the tuning process, generates velocity data

clear all;
close all;
% clc;


%%
% User chooses file in folder

%fileName = uigetfile({'*.txt'})%,'\\CANAS01\Share\SanDiego\Users\JoshS\MATLAB');
%fileName = '20160929Text.txt';

%fileID = fopen('TRACE.txt');

% Get File from UI
[fileName, PathName, FilterIndex] = uigetfile({'*.txt'}, 'Choose Trace', 'C:\Firefly EOL Testing\Test Results\TraceResults');
[pathstr,fileNameStr,ext] = fileparts(fileName); % save name without extension
fileID = fopen(fullfile(PathName, fileName));


fileData = textscan(fileID, '%s', 'Delimiter', '\n');
thename = fileData{1}{5};
themot = fileData{1}{6};
stpos = fileData{1}{7};
endpos = fileData{1}{8};
kp = fileData{1}{9};
ki = fileData{1}{10};
kd = fileData{1}{11};
id1 = fileData{1}{12};
id2 = fileData{1}{13};
id3 = fileData{1}{14};
id4 = fileData{1}{15};
id5 = fileData{1}{16};
id6 = fileData{1}{17};
hostname1 = textscan(id1, 'ID: hostname = %s');
hostname = hostname1{1}{1};

frewind(fileID);


%%
% Parse data from file
fileData = textscan(fileID, '%d %d %d %d%*c %*d','headerlines',18);
time = fileData{1};
commandPosition = fileData{2};
outputPosition = fileData{3};
pwm = abs(fileData{4});

%%
% Calculations
posErr = abs(outputPosition - commandPosition);
z = zeros(length(posErr));

commandVelocity = abs(diff(commandPosition));
outputVelocity = abs(diff(outputPosition));

%%
% Running average calculation
szpwm = length(pwm);
nSamples = 250;
nPre = ceil((nSamples-1)/2); % number of samples to use in average before indexed location
nPost = nSamples - 1 - nPre; % number of samples to use in average after indexed location

avgIdxRange = nPre+1:szpwm-nPost; % indexes at which average calculation carried out

avg = zeros(size(avgIdxRange));
for i = avgIdxRange % For each point that we want the average
	% get the sum of all the points before and after
	sum = 0;
	for j = i-nPre:i+nPost
		sum = sum + pwm(j);
	end
	avg(i-nPre)=sum/nSamples;
	% disp(strcat(num2str(i),"\t",num2str(sum)))
end


%%
% Plot
figure('Name', fileName,'position', [0,50,1500,950])

tb = uicontrol('style', 'text');
mystr = {thename;
	' ';
	'Motor =';
	themot;
	' ';
	'Start Pos =';
	stpos;
	' ';
	'End Pos =';
	endpos;
	' ';
	'Kp =';
	kp;
	' ';
	'Ki =';
	ki;
	' ';
	'Kd=';
	kd;
	' ';
	id1;
	' ';
	id2;
	' ';
	id3;
	' ';
	id4;
	' ';
	id5;
	' ';
	id6;
	' ';
	'nSamples for Avg =';
	nSamples};
set(tb,'String',mystr);
set(tb,'HorizontalAlignment', 'left')
set(tb, 'Position', [15 50 130 850]);
%set(tb, 'BackgroundColor', 'green');
set(tb, 'FontSize', 10);

subplot(4,1,1);
hold on
plot(outputPosition)
plot(commandPosition, 'r')
title('Position Profile')
ylabel('Encoder Position')
hold off

subplot(4,1,2);
hold on
plot(outputVelocity)
plot(commandVelocity, 'r')
title('Velocity Profile (abs)')
ylim([-5 40])
ylabel('Encoder Velocity')
hold off

subplot(4,1,3);
hold on
plot(posErr)
plot(z,'r')
ylim([-10 150])
title('Position Error (abs)')
ylabel('Error (Counts)')
hold off

subplot(4,1,4);
hold on
plot(pwm)
plot(avgIdxRange, avg, 'k')
plot(z,'r')
ylim([-100 100])
title('PWM (abs) and Running Average')
ylabel('PWM (%)')
legend('pwm','Avg')
hold off

%%
% Output to file
% fileName = strcat(hostname,'_',thename,'.jpg');%,'_',stpos,'to',endpos,'_',kp,'-',ki,'-',kd,'.jpg')

% filePath = strcat('TraceResults\', fileName);
% saveas(gcf,filePath);
fileName = strcat(fileNameStr, '.jpg');
saveas(gcf,fullfile(PathName, fileName));

%%
% Close the file
fclose(fileID);
